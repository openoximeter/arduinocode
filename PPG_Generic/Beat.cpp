#include "Beat.h"

// === Windowed DC Filter =================================================

/**
 * A simple rolling window DC filter.
 */
DCFilter::DCFilter(void) {
    windowed_sum = 0;
}    

/**
 * Filters DC from the passed samples by removing the mean value.
 * 
 * Uses a large window defined by DC_WINDOW_LENGTH in "Beat.h".
 * 
 * @param sample The sample to be filtered.
 * @return int16_t
 */
int16_t DCFilter::filter(int32_t sample) {
    // Add new sample and remove 1/N of previous sum.
    windowed_sum += (sample - windowed_sum/DC_WINDOW_LENGTH);

    // Filter by removing the mean value from the signal
    return (int16_t)(sample - windowed_sum/DC_WINDOW_LENGTH);
}

/**
 * Returns the current DC value for the windowed samples.
 * 
 * @return int32_t
 */
int32_t DCFilter::avgDC() {
    return windowed_sum/DC_WINDOW_LENGTH;
}

// === Butterworth Filter =================================================
// See RollingAverageByte.h for potential (optimized) replacement implementation (NOTE: This only supports bytes...)

/**
 * A simple butterworth filter.
 * http://www.schwietering.com/jayduino/filtuino/
 * Low pass butterworth filter order=1 alpha1=0.1
 * Fs=100Hz, Fc=6Hz
 */
MAFilter::MAFilter(void) {
    v[0]=0.0;
}

/**
 * Filters the sample, using a combination of the passed sample and stored values.
 * 
 * @param sample The sample to be filtered.
 * @return float
 */
float MAFilter::filter(float sample) {
    v[0] = v[1];
			v[1] = (2.452372752527856026e-1 * sample)
				 + (0.50952544949442879485 * v[0]);
			return
				 (v[0] + v[1]);
}

// === Beat Class to track and detect heart rate ==========================

/**
 * Beat class handles signal filtering, processing and heart rate detection.
 */
Beat::Beat(void) :
    state(BEATDETECTOR_STATE_INIT),
    threshold(BEATDETECTOR_MIN_THRESHOLD),
    beatPeriod(0),
    lastMaxValue(0),
    tsLastBeat(0)
{
    cycle_max = 20;
    cycle_min = -20;
    positive = false;
    prev_sig = 0;
    amplitude_avg_total = 0;
};

/**
 * Filters DC out of the sample
 * 
 * @param sample The sample to be filtered.
 * @return int16_t
 */
int16_t Beat::dc_filter(int32_t sample) {
    return dc.filter(sample);
}

/**
 * Applies a low pass moving average filter to the sample
 * 
 * @param sample The sample to be filtered.
 * @return int16_t
 */
int16_t Beat::ma_filter(int16_t sample) {
    return ma.filter(sample);
}

/**
 * Returns true when a heart beat is detected.
 * 
 * @param signal The signal that is to be searched.
 * @return bool
 */
bool Beat::isBeat(float signal) {

    // THE FOLLOWING CODE IS NEEDED TO CALCULATE amplitude_avg_total
    // while positive slope record maximum
    if (positive && (signal > prev_sig)) cycle_max = signal;  
    // while negative slope record minimum
    if (!positive && (signal < prev_sig)) cycle_min = signal;
    //  positive to negative
    if (positive && (signal < prev_sig)) {
        int amplitude = cycle_max - cycle_min;
        if (amplitude > 20 && amplitude < 3000) {
            amplitude_avg_total += (amplitude - amplitude_avg_total/4); 
            }
            cycle_min = 0; positive = false;
    } 
    //negative to positive i.e valley bottom 
    if (!positive && (signal > prev_sig)) {
        cycle_max= 0; positive = true;
    } 
    prev_sig = signal; // save signal

    return checkForBeat(signal);
}

/**
 * Returns the average DC level for the filtered signal thus far.
 * 
 * @return int32_t
 */
int32_t Beat::avgDC(void) {
    return dc.avgDC();
}

/**
 * Returns the average AC level for the filtered signal.
 * 
 * @return int16_t
 */
int16_t Beat::avgAC(void) {
    return amplitude_avg_total/4;
}

/**
 * Returns the current threshold used in beat detection.
 * 
 * @returns float
 */
float Beat::getCurrentThreshold()
{
    return threshold;
}

/**
 * Returns the current heart rate.
 * 
 * @returns uint8_t
 */
uint8_t Beat::getRate(void) {
    if (beatPeriod != 0) {
        return 1 / beatPeriod * 1000 * 60;
    } else {
        return 0;
    }
}

BeatDetectorState Beat::getState(void) {
    return state;
}

/**
 * Checks for a beat and returns true if one is detected.
 * 
 * @param sample The sample that may contain a beat
 * @return bool
 */
bool Beat::checkForBeat(float sample) {
    bool beatDetected = false;

    switch (state) {
        case BEATDETECTOR_STATE_INIT:
            // If the system has been on for more than 2 seconds, move to waiting state.
            if (millis() > BEATDETECTOR_INIT_HOLDOFF) {
                state = BEATDETECTOR_STATE_WAITING;
            }
            break;
        
        case BEATDETECTOR_STATE_WAITING:
            // Only activate the beat detector if the threshold value is exceeded
            if (sample >= threshold) {
                threshold = min(sample, BEATDETECTOR_MAX_THRESHOLD);
                state = BEATDETECTOR_STATE_FOLLOWING_SLOPE;
            }
            // If tracking is lost, reset the stored values and decrease the threshold
            if (millis() - tsLastBeat > BEATDETECTOR_INVALID_READOUT_DELAY) {
                beatPeriod = 0;
                lastMaxValue = 0;
            }
            decreaseThreshold(sample);
            break;

        case BEATDETECTOR_STATE_FOLLOWING_SLOPE:
            // Follow the slope until the samples start falling again
            if (sample < threshold) {
                state = BEATDETECTOR_STATE_MAYBE_DETECTED;
            } else {
                threshold = min(sample, BEATDETECTOR_MAX_THRESHOLD);
            }
            break;

        case BEATDETECTOR_STATE_MAYBE_DETECTED:
            // Sample has risen and fallen... so...
            if (sample + BEATDETECTOR_STEP_RESILIENCY < threshold) {
                // Found a beat
                beatDetected = true;
                lastMaxValue = sample;
                state = BEATDETECTOR_STATE_MASKING;
                float delta = millis() - tsLastBeat;
                if (delta) {
                    beatPeriod = BEATDETECTOR_BPFILTER_ALPHA * delta +
                            (1 - BEATDETECTOR_BPFILTER_ALPHA) * beatPeriod;
                }

                tsLastBeat = millis();
            } else {
                state = BEATDETECTOR_STATE_FOLLOWING_SLOPE;
            }
            break;

        case BEATDETECTOR_STATE_MASKING:
            // Avoid double triggering (think hyperpolarization)
            if (millis() - tsLastBeat > BEATDETECTOR_MASKING_HOLDOFF) {
                state = BEATDETECTOR_STATE_WAITING;
            }
            decreaseThreshold(sample);
            break;
    }

    return beatDetected;
}

/**
 * Finetunes the threshold using internal state information to guide the scale factor.
 */
void Beat::decreaseThreshold(float sample)
{
    float diff_scale = (threshold - sample)/BEATDETECTOR_THRESHOLD_SCALER;

    // When a valid beat rate readout is present, target the previous reading point
    if (lastMaxValue > 0 && beatPeriod > 0) {
        threshold -= diff_scale * lastMaxValue * (1 - BEATDETECTOR_THRESHOLD_FALLOFF_TARGET) /
                (beatPeriod / BEATDETECTOR_SAMPLES_PERIOD);
    } else {
        // Asymptotic decay
        threshold *= BEATDETECTOR_THRESHOLD_DECAY_FACTOR;
    }

    // Apply lower limit if needed.
    if (threshold < BEATDETECTOR_MIN_THRESHOLD) {
        threshold = BEATDETECTOR_MIN_THRESHOLD;
    }
}