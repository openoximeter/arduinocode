#include <arduino.h>

//#define DEBUG_ROLLING_DATA

class RollingAverageByte{
    public:
        RollingAverageByte(int length);
        void clear(void);
        void store(uint8_t data);
        int getAverage(void);

        #ifdef DEBUG_ROLLING_DATA
            void PrintRollingData(void);
        #endif

    private:
        int allocateMemory();
        int freeMemory();
        uint16_t _sum;
        uint8_t * _RollingData;
        uint8_t _length;
        uint8_t _shift;
    
};