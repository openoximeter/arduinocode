#include "Wire.h"
#include "MAX3010x.h"

MAX3010x::MAX3010x() {
  // Constructor
}

boolean MAX3010x::begin(uint8_t i2caddr) {
  _i2caddr = i2caddr;
  if (readRegister8(REG_PART_ID) != MAX_PART_ID)  return false; 
  return true;
}

void MAX3010x::setup() {
    writeRegister8(REG_MODE_CONFIG,0x40); //reset
    delay(500);
    writeRegister8(REG_FIFO_WR_PTR,0x00); // Reset FIFO write pointer
    writeRegister8(REG_OVF_COUNTER,0x00); // Reset FIFO overflow
    writeRegister8(REG_FIFO_RD_PTR,0x00); // Reset FIFO read pointer
    #ifdef REG_FIFO_CONFIG
        writeRegister8(REG_FIFO_CONFIG,0x4f); //sample avg = 4, fifo rollover=false, fifo almost full = 17
    #endif
    writeRegister8(REG_MODE_CONFIG,0x03); //0x02 for Red only, 0x03 for SpO2 mode.
    #ifdef MAX30100
        // TODO: Determine if SPO2 High Resolution is required (enabled at bit 6)
        writeRegister8(REG_SPO2_CONFIG,0b01000101);
    #else
        writeRegister8(REG_SPO2_CONFIG,0x27); // SPO2_ADC=4096nA, SPO2 sample rate(100Hz), pulseWidth (411uS)
    #endif
    
    // Set LED current limits
    #ifdef REG_LED_PA
        writeRegister8(REG_LED_PA, 0x77); // Set LED current
    #endif

    #ifdef REG_LED1_PA
        writeRegister8(REG_LED1_PA,0x17); //Choose value for ~ 6mA for LED1 (IR)
    #endif

    #ifdef REG_LED2_PA
        writeRegister8(REG_LED2_PA,0x17); // Choose value for ~ 6mA for LED2 (Red)
    #endif

    #ifdef REG_LED3_PA
        writeRegister8(REG_LED3_PA,0x17); // Choose value for ~ 6mA for Pilot LED
    #endif

    #ifdef REG_LED4_PA
        writeRegister8(REG_LED4_PA,0x17); // Choose value for ~ 6mA for Pilot LED
    #endif

    #ifdef REG_PILOT_PA
        writeRegister8(REG_PILOT_PA,0x1F); // Choose value for ~ 6mA for Pilot LED
    #endif
}

//Tell caller how many samples are available
uint8_t MAX3010x::available(void) {
  int8_t numberOfSamples = sense.head - sense.tail;
  if (numberOfSamples < 0) numberOfSamples += STORAGE_SIZE;
  return (numberOfSamples);
}

//Report the next Red value in the FIFO
uint32_t MAX3010x::getRed(void) {
  return (sense.red[sense.tail]);
}

//Report the next IR value in the FIFO
uint32_t MAX3010x::getIR(void) {
  return (sense.IR[sense.tail]);
}

//Advance the tail
void MAX3010x::nextSample(void) {
  if(available()) {
    sense.tail++;
    sense.tail %= STORAGE_SIZE; //Wrap condition
  }
}

// check sensor for new samples and upload if available
uint16_t MAX3010x::check(void) {
  byte readPointer = readRegister8(REG_FIFO_RD_PTR);
  byte writePointer = readRegister8(REG_FIFO_WR_PTR);
  int numberOfSamples = 0;
  if (readPointer != writePointer) {
    //Calculate the number of readings we need to get from sensor
    numberOfSamples = writePointer - readPointer;
    if (numberOfSamples < 0) numberOfSamples += FIFO_DEPTH; //Wrap condition
    int bytesLeftToRead = numberOfSamples * STORAGE_SIZE * 2; //STORAGE_SIZE bytes each for Red and IR    
    Wire.beginTransmission(_i2caddr);
    Wire.write(REG_FIFO_DATA);
    Wire.endTransmission();
    bytesLeftToRead = bytesLeftToRead<=FIFO_DEPTH? bytesLeftToRead : FIFO_DEPTH;
    Wire.requestFrom(_i2caddr, bytesLeftToRead);      
    while (bytesLeftToRead > 0) {
        sense.head++; //Advance the head of the storage struct
        sense.head %= STORAGE_SIZE; //Wrap condition
        #ifdef MAX30100
            sense.IR[sense.head] = readFIFOSample(); 
            //Burst read three more bytes - IR  
            sense.red[sense.head] = readFIFOSample();
        #else 
            sense.red[sense.head] = readFIFOSample(); 
            //Burst read three more bytes - IR  
            sense.IR[sense.head] = readFIFOSample();
        #endif
        bytesLeftToRead -= STORAGE_SIZE * 2;
    }
    Wire.endTransmission();
  } 
  return (numberOfSamples);
}

//
// Low-level I2C Communication
//
uint8_t MAX3010x::readRegister8(uint8_t reg) {
    uint8_t value;
    Wire.beginTransmission(_i2caddr);
    Wire.write((uint8_t)reg);
    Wire.endTransmission();
    Wire.requestFrom(_i2caddr, (byte)1);
    value = Wire.read();
    Wire.endTransmission();
    return value;
}

uint32_t MAX3010x::readFIFOSample() {
    byte temp[4]; 
    uint32_t temp32;
    temp[3] = 0;
    #if (STORAGE_SIZE==3)
        temp[2] = Wire.read();
    #else 
        temp[2] = 0;
    #endif
    temp[1] = Wire.read();
    temp[0] = Wire.read();
    memcpy(&temp32, temp, 4);	
    return temp32 & 0x3FFFF;	
}

void MAX3010x::writeRegister8(uint8_t reg, uint8_t value) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}