#include "Arduino.h"
// #include "RollingAverageByte.h"

#ifndef min
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
#endif

// === Filters used to clean signals ============================================

#define DC_WINDOW_LENGTH 24  // Moving Average DC removal Filter
#define MA_WINDOW_LENGTH 4   // Simple Moving Average Filter

class DCFilter {
    public:
        DCFilter(void);                     // DC Filter Constructor
        int16_t filter(int32_t sample);     // Remove DC from sample
        int32_t avgDC(void);                // Return average DC
        
    private:   
        int32_t windowed_sum;               // Rolling total (for N samples)
};

class MAFilter {
    public:
        MAFilter(void);                     // MA Filter Constructor
        float filter(float sample);       // Filter the sample

    private:
        float v[2];
};

// === Beat Class used to track and capture heart rate ==========================

/*
Arduino-MAX30100 oximetry / heart rate integrated sensor library
Copyright (C) 2016  OXullo Intersecans <x@brainrapers.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define BEATDETECTOR_INIT_HOLDOFF                2000    // in ms, how long to wait before counting
#define BEATDETECTOR_MASKING_HOLDOFF             200     // in ms, non-retriggerable window after beat detection
#define BEATDETECTOR_BPFILTER_ALPHA              0.6     // EMA factor for the beat period value
#define BEATDETECTOR_MIN_THRESHOLD               20      // minimum threshold (filtered) value
#define BEATDETECTOR_MAX_THRESHOLD               500     // maximum threshold (filtered) value (800)
#define BEATDETECTOR_STEP_RESILIENCY             20      // maximum negative jump that triggers the beat edge
#define BEATDETECTOR_THRESHOLD_FALLOFF_TARGET    0.1     // thr chasing factor of the max value when beat (0.3)
#define BEATDETECTOR_THRESHOLD_DECAY_FACTOR      0.99    // thr chasing factor when no beat
#define BEATDETECTOR_INVALID_READOUT_DELAY       2000    // in ms, no-beat time to cause a reset
#define BEATDETECTOR_SAMPLES_PERIOD              10      // in ms, 1/Fs
#define BEATDETECTOR_THRESHOLD_SCALER            25      // The target lag between threshold and samples.

typedef enum BeatDetectorState {
    BEATDETECTOR_STATE_INIT,
    BEATDETECTOR_STATE_WAITING,
    BEATDETECTOR_STATE_FOLLOWING_SLOPE,
    BEATDETECTOR_STATE_MAYBE_DETECTED,
    BEATDETECTOR_STATE_MASKING
} BeatDetectorState;

class Beat {
    public:
        Beat(void);
        int16_t dc_filter(int32_t sample);  // Remove DC from sample
        int16_t ma_filter(int16_t sample);  // Low pass filter sample
        bool isBeat(float signal);          // Return true when a beat is detected
        int32_t avgDC(void);                // Return the average DC level
        int16_t avgAC(void);                // Return the average AC level
        float getCurrentThreshold(void);    // Returns the current threshold for the state machine detection
        uint8_t getRate(void);              // Returns the current detected heart rate
        BeatDetectorState getState(void);

    private:
        DCFilter dc;
        MAFilter ma;
        int16_t amplitude_avg_total;
        int16_t cycle_max;
        int16_t cycle_min;
        bool positive;
        int16_t prev_sig;
        
        bool checkForBeat(float sample);                // Checks for a beat using the state machine approach
        void decreaseThreshold(float sample);           // ???

        BeatDetectorState state;
        float threshold;
        float beatPeriod;
        float lastMaxValue;
        uint32_t tsLastBeat;
};