#include "ssd1306h.h"
#include "MAX3010x.h"
#include "Beat.h"
#include "RollingAverageByte.h"
#include <avr/pgmspace.h>
#include <avr/sleep.h>

// #define SERIAL_PLOT_RAW_DATA

// Routines to clear and set bits 
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

SSD1306 oled; 
MAX3010x sensor;
Beat pulseIR;
Beat pulseRed;
MAFilter bpm;

#define LED LED_BUILTIN
#define OPTIONS 7

#define SPO2THRESH  95
#define ALARMTIME 500

#define DOWNARROW 128
#define UPARROW 129

// heart symbol for screen
static const uint8_t heart_bits[] PROGMEM = { 0x00, 0x00, 0x38, 0x38, 0x7c, 0x7c, 0xfe, 0xfe, 0xfe, 0xff, 
                                        0xfe, 0xff, 0xfc, 0x7f, 0xf8, 0x3f, 0xf0, 0x1f, 0xe0, 0x0f,
                                        0xc0, 0x07, 0x80, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 
                                        0x00, 0x00 };

//spo2_table is approximated as  -45.060*ratioAverage* ratioAverage + 30.354 *ratioAverage + 94.845 ;
const uint8_t spo2_table[184] PROGMEM =
        { 95, 95, 95, 96, 96, 96, 97, 97, 97, 97, 97, 98, 98, 98, 98, 98, 99, 99, 99, 99, 
          99, 99, 99, 99, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 
          100, 100, 100, 100, 99, 99, 99, 99, 99, 99, 99, 99, 98, 98, 98, 98, 98, 98, 97, 97, 
          97, 97, 96, 96, 96, 96, 95, 95, 95, 94, 94, 94, 93, 93, 93, 92, 92, 92, 91, 91, 
          90, 90, 89, 89, 89, 88, 88, 87, 87, 86, 86, 85, 85, 84, 84, 83, 82, 82, 81, 81, 
          80, 80, 79, 78, 78, 77, 76, 76, 75, 74, 74, 73, 72, 72, 71, 70, 69, 69, 68, 67, 
          66, 66, 65, 64, 63, 62, 62, 61, 60, 59, 58, 57, 56, 56, 55, 54, 53, 52, 51, 50, 
          49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 31, 30, 29, 
          28, 27, 26, 25, 23, 22, 21, 20, 19, 17, 16, 15, 14, 12, 11, 10, 9, 7, 6, 5, 
          3, 2, 1 } ;


int getVCC() {
  //reads internal 1V1 reference against VCC
  #if defined(__AVR_ATmega1284P__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);  // For ATmega1284
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);  // For ATmega328
  #endif
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  uint8_t low = ADCL;
  unsigned int val = (ADCH << 8) | low;
  //discard previous result
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  low = ADCL;
  val = (ADCH << 8) | low;
  
  return (((long)1024 * 1100) / val)/100;  
}

void print_digit(int x, int y, long val, char c=' ', uint8_t field = 3,
                    const int BIG = 2){  
    uint8_t ff = field;
    do { 
        char ch = (val!=0) ? val%10+'0': c;
        oled.drawChar( x+BIG*(ff-1)*6, y, ch, BIG);
        val = val/10; 
        --ff;
    } while (ff>0);
}


/*
 *   Record, scale  and display PPG Wavefoem
 */
const uint8_t MAXWAVE = 72;

class Waveform {
  public:
    Waveform(void) {wavep = 0;}

      void record(int waveval) {
        waveval = waveval/8;         // scale to fit in byte
        waveval += 128;              //shift so entire waveform is +ve
        waveval = waveval<0? 0 : waveval;
        waveform[wavep] = (uint8_t) (waveval>255)?255:waveval; 
        wavep = (wavep+1) % MAXWAVE;
      }
  
      void scale() {
        uint8_t maxw = 0;
        uint8_t minw = 255;
        for (int i=0; i<MAXWAVE; i++) { 
          maxw = waveform[i]>maxw?waveform[i]:maxw;
          minw = waveform[i]<minw?waveform[i]:minw;
        }
        uint8_t scale8 = (maxw-minw)/4 + 1;  //scale * 8 to preserve precision
        uint8_t index = wavep;
        for (int i=0; i<MAXWAVE; i++) {
          disp_wave[i] = 31 - ((uint16_t)(waveform[index]-minw)*8)/scale8;
          index = (index + 1) % MAXWAVE;
        }
      }

      void draw(uint8_t X) {
       for (int i=0; i<MAXWAVE; i++) {
        uint8_t y = disp_wave[i];
        oled.drawPixel(X+i, y);
        if (i<MAXWAVE-1) {
          uint8_t nexty = disp_wave[i+1];
          if (nexty>y) {
            for (uint8_t iy = y+1; iy<nexty; ++iy)  oled.drawPixel(X+i, iy);
          } else if (nexty<y) {
            for (uint8_t iy = nexty+1; iy<y; ++iy)  oled.drawPixel(X+i, iy);
          }
        }
      }  
    }

private:
    uint8_t waveform[MAXWAVE];
    uint8_t disp_wave[MAXWAVE];
    uint8_t wavep = 0;
    
} wave;

RollingAverageByte rollHR(4);
RollingAverageByte rollSpO2(4);

int  beatAvg;
int  SPO2;
int  voltage;
bool filter_for_graph = true;
bool draw_Red = true;
uint8_t pcflag =0;
uint8_t istate = 0;


long alarmtime = 0;   // Time of the last alarm update
bool alarmInvert = false;

/**
 * Flash the screen to alert the user that there is an issue.
 * 
 * @param time Current time in milliseconds.
 * @return void.
 */
void displayAlarm(long time = millis()) {
  if (time - alarmtime>ALARMTIME) {
      alarmtime = time;
      alarmInvert = !alarmInvert;
      oled.invertDisplay(alarmInvert);
  }
}

void draw_oled(int msg) {
    oled.firstPage();
    do{
    switch(msg){
        // device error
        case 0:  oled.drawStr(10,0,F("Device error"),1);
                 oled.drawChar(100,0,voltage/10+'0');
                 oled.drawChar(106,0,'.');
                 oled.drawChar(112,0,voltage%10+'0');
                 oled.drawChar(118,0,'V');
                 break;
        // no finger detected
        case 1:  oled.drawStr(13,10,F("PLACE"),1); 
                 oled.drawStr(10,20,F("FINGER"),1);
                 oled.drawChar(100,0,voltage/10+'0');
                 oled.drawChar(106,0,'.');
                 oled.drawChar(112,0,voltage%10+'0');
                 oled.drawChar(118,0,'V');
                 oled.drawStr(84,14,F("Display"),1); 
                 if (draw_Red) 
                    oled.drawStr(84,24,F("Red"),1); 
                 else
                    oled.drawStr(84,24,F("IR"),1); 
                 if (filter_for_graph) 
                    oled.drawStr(108,24,F("Avg"),1); 
                 else
                    oled.drawStr(108,24,F("Raw"),1); 

                 oled.drawChar(80,0,DOWNARROW); // Down arrow
                 oled.drawChar(70,0,UPARROW);   // Up arrow
                 oled.drawChar(60,0,'-');       // No trend
                 break;
        // standard display operation
        case 2:  print_digit(80,0,rollHR.getAverage());
                 // oled.drawChar(116,0,UPARROW,2);                    // TODO: This needs to be calculated and replaced!
                 wave.draw(8);
                 print_digit(80,16,rollSpO2.getAverage(),' ',3,2);
                 oled.drawChar(116,16,'%',2);
                 break;
        // boot screen
        case 3:  oled.drawStr(28,12,F("UoB Pulse-Ox"),1);
                 oled.drawXBMP(6,8,16,16,heart_bits);
                 oled.drawXBMP(106,8,16,16,heart_bits);
                 break;
        }
    } while (oled.nextPage());
}

void setup(void) {
  pinMode(LED, OUTPUT);
  oled.init();
  oled.fill(0x00);
  draw_oled(3);
  delay(3000); 
  if (!sensor.begin())  {
    voltage = getVCC();
    while(1) {
      displayAlarm();
      draw_oled(0);
    }
  }
  oled.invertDisplay(false);
  sensor.setup();
  #ifdef SERIAL_PLOT_RAW_DATA
    Serial.begin(19200);
  #endif
}

long lastBeat = 0;    //Time of the last beat 
long displaytime = 0; //Time of the last display update
bool led_on = false;

void loop()  {
    // Access and read sensor FIFO
    sensor.check();
    long now = millis();   //start time of this cycle
    if (!sensor.available()) return;
    // Get raw values from the sensor
    uint32_t redValue = sensor.getRed(); 
    uint32_t irValue = sensor.getIR();
    sensor.nextSample();

    // Check if finger is on the sensor.
    if (irValue<5000) {
        // Get voltage for display message
        voltage = getVCC();
        // Clear inversion of screen
        if (alarmInvert == true) {
          alarmInvert = false;
          oled.invertDisplay(false);
        }
        // Clear stored data from rolling averages
        rollHR.clear();
        rollSpO2.clear();

        draw_oled(1); // finger not down message
    
    // If finger is on sensor...
    } else {
        float IR_signal, Red_signal;
        bool beatRed, beatIR;

        // If data should be unfiltered...
        if (!filter_for_graph) {
          // Remove just the DC element from the signal
          IR_signal =  pulseIR.dc_filter(irValue) ;
          Red_signal = pulseRed.dc_filter(redValue);
          // Filter and detect if beat has occurred
          beatRed = pulseRed.isBeat(-pulseRed.ma_filter(Red_signal));      // -
          beatIR =  pulseIR.isBeat(-pulseIR.ma_filter(IR_signal));         // -
        } else {

          // Filter the signal using a 4-point moving average filter
          IR_signal =  -pulseIR.ma_filter(pulseIR.dc_filter(irValue)) ;    // -
          Red_signal = -pulseRed.ma_filter(pulseRed.dc_filter(redValue));  // -
          // Detect if beat has occurred
          beatRed = pulseRed.isBeat(Red_signal);
          beatIR =  pulseIR.isBeat(IR_signal);
          #ifdef SERIAL_PLOT_RAW_DATA
            Serial.print(Red_signal);
            Serial.print(",");
            Serial.print(beatRed*200);
            Serial.print(",");
            Serial.print(pulseRed.getState()*100);
            Serial.print(",");
            Serial.println(pulseRed.getCurrentThreshold());
          #endif
        }

        
        
        // Plot data to screen
        wave.record(draw_Red ? Red_signal : IR_signal ); 

        // check IR or Red for heartbeat     
        if (draw_Red ? beatRed : beatIR){
            // calculate BPM
            long btpm = 60000/(now - lastBeat);
            if (btpm > 0 && btpm < 200) beatAvg = bpm.filter((int16_t)btpm);
            lastBeat = now;
            
            // Flash LED
            digitalWrite(LED, HIGH); 
            led_on = true;

            // compute SpO2 ratio
            long numerator   = (pulseRed.avgAC() * pulseIR.avgDC())/256;
            long denominator = (pulseRed.avgDC() * pulseIR.avgAC())/256;
            int RX100 = (denominator>0) ? (numerator * 100)/denominator : 999;

            // calc SpO2 from table using ratio
            if ((RX100>=0) && (RX100<184)) {
              SPO2 = pgm_read_byte_near(&spo2_table[RX100]);
            }
            
            // store BPM and SpO2 in rolling average filter
            rollHR.store((uint8_t) pulseRed.getRate());
            rollSpO2.store((uint8_t) SPO2);
        }

        // update display every 50 ms if fingerdown
        if (now-displaytime>50) {
            displaytime = now;
            wave.scale();
            draw_oled(2);
        }

        // Flash display if SPO2 is too low
        if (SPO2 < SPO2THRESH) {
          displayAlarm();
        } else if (alarmInvert == true) {
          alarmInvert = false;
          oled.invertDisplay(false);
        }
    }
    // flash led for 25 ms
    if (led_on && (now - lastBeat)>25){
        digitalWrite(LED, LOW);
        led_on = false;
     }
}
