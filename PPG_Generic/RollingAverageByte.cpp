#include "RollingAverageByte.h"

/**
 * A class that stores 8-bit data and calculates the rolling average.
 * 
 * @param length Length of array for rolling average, MUST =2^N!
 * @return RollingAverageByte
 */
RollingAverageByte::RollingAverageByte(int length) {
    _length = length;
    _shift = log (length) / log (2);
    allocateMemory();
    clear();
}

/**
 * Allocates the memory for the rolling average data.
 * 
 * @return void
 */
int RollingAverageByte::allocateMemory() {
    _RollingData = new uint8_t[_length];
}

/**
 * Deallocates the memory for the rolling average data.
 * 
 * @return void
 */
int RollingAverageByte::freeMemory() {
    delete[] _RollingData;
}

/**
 * Clears any stored data in memory.
 * 
 * @return void
 */
void RollingAverageByte::clear(void) {
    _sum = 0;
    for(int i = 0; i < _length; i++){
        _RollingData[i] = 0;
    }
}

/**
 * Appends a new data point into the rolling average memory.
 * 
 * @return void
 */
void RollingAverageByte::store(uint8_t data) {
    _sum -= _RollingData[_length-1];
    _sum += data;

    for(int i = _length - 1; i > 0; i--){
        _RollingData[i] = _RollingData[i-1];
    }
    _RollingData[0] = data;

    #ifdef DEBUG
        PrintRollingData();
    #endif
}

/**
 * Calculates the average using the rolling data in memory.
 * 
 * @return int
 */
int RollingAverageByte::getAverage(void) {
    return (int) _sum >> _shift;
}

#ifdef DEBUG_ROLLING_DATA
    /**
     * Prints the classes internal variables to serial port for debug.
     * (must be enabled by defining DEBUG_ROLLING_DATA in RollingAverageByte.h)
     * @return void
     */
    void RollingAverageByte::PrintRollingData(void) {
        Serial.println(_length);
        Serial.println(_shift);
        for(int i = 0; i < _length; i++){
            Serial.print(_RollingData[i]);
            Serial.print(", ");
        }
        Serial.println();
        Serial.println(_sum);
    }
#endif

