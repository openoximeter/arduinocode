**The OpenOximeter is currently in an early prototype stage. It is never safe to build your own medical equipment.**
**This project is open to encourage experts to work together to build the best and most adaptable solution.**

This repository contains the code for use on any of the University of Bath pulse-oximeter sensors.

## SSD1306H Display
The ssd1306h.h library is a modified version of the reduced SSD1306h library originally produced by [jeffmer](https://github.com/jeffmer/tinyPulsePPG). This version has additional support for inverted displays.
The original library was released under an Un-licence meaning it is free for unrestricted use and modification.

## MAX3010x Library
This library was written from the ground-up for this project, to provide equivalent operation between the entire MAX3010x family.

In order to use this code with your specific MAX3010x devcice, you must first modify the define statement at the top of this library file.

## Install Procedure
This repository has been setup to enable easy and rapid install. All required code is contained within these project files and there is no need to install additional libraries or systems. The following instructions assume that you have installed a version of the Arduino IDE, available from the [Arduino software downloads page](https://www.arduino.cc/en/software).

1) Clone this repo onto your local machine using `git clone https://gitlab.com/openoximeter/arduinocode`
2) Open the `./PPG_Generic/PPG_Generic.ino` file in the Arduino IDE.
    - This will open the main project file, along with the various library files included in this project as separate tabs within the IDE.
3) Navigate to the `MAX3010x.h` tab and change the define statement found on line 3 to match your MAX device. Accepted settings are:
    - `#define MAX30100` for the MAC30100 chipset.
    - `#define MAX30101` for the MAC30101 chipset.
    - `#define MAX30102` for the MAC30102 chipset.
    - `#define MAX30105` for the MAC30105 chipset.
4) Save this change and navigate back to the main `PPG_Generic.ino` tab.
5) Verify and upload the code onto your target Arduino device. (Note: In some cases, the verify operation appears to fail on first run - if this happens, attempt an upload anyway!)

## TODO

Required:
- Trend Calculation (Symbols already supported)

Optional:
- Rolling average ready flag (to avoid the soft startup on readings)
- Dynamic LED current limits - look into shifting the current limit depending on the raw brightness of the incoming signal to save power. How will this affect reading quality?
